export type Post = {
    userId: string
    id: number
    title: string
    body: string
}

export async function fetchPost(delay: number, id: number): Promise<Post> {
    await new Promise(resolve => setTimeout(resolve, delay))
    const res = await fetch(
        `https://jsonplaceholder.typicode.com/posts/${id}`,
        {cache: 'no-store'}
    )
    return res.json()
}
