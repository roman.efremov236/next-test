import React, {Suspense} from 'react'
import Post from '@/app/_components/Post'
import Button from '@/app/_components/Button'

export default async function Page() {
    return (
        <div>
            <h2 className='text-xl'>Static text</h2>
            <div>
                <p>text</p>
                <p>another text</p>
            </div>
            <hr className='my-4'/>
            <Button text="button"/>
            <div>
                <Suspense fallback={<div className='text-red-500'>Loading...</div>}>
                    <Post delay={2000} id={1}/>
                </Suspense>
                <Suspense fallback={<div className='text-red-500'>Loading...</div>}>
                    <Post delay={3000} id={2}/>
                </Suspense>
                <Suspense fallback={<div className='text-red-500'>Loading...</div>}>
                    <Post delay={4000} id={3}/>
                </Suspense>
            </div>
        </div>
    )
}
