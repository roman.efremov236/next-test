import ClientCounter from '@/app/_components/ClientCounter'

export default function Home() {
    return (
        <main className='p-4'>
            <h1 className='text-xl'>Hello world</h1>
            <ClientCounter text='page counter'/>
        </main>
    )
}
