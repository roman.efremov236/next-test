import {fetchPost} from '@/app/_lib/data'

export default async function Post({delay, id}: { delay: number, id: number }) {
    const post = await fetchPost(delay, id)

    return <div>{post.title}</div>
}
