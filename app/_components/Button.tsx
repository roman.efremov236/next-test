export default function Button({text}: { text: string }) {
    return (
        <button className='p-4 bg-amber-500'>{text}</button>
    )
}