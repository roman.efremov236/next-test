import Link from 'next/link'

export default function Navbar() {
    return (
        <div className='flex flex-row space-x-4 p-4 bg-gradient-to-r from-cyan-500 to-blue-500'>
            <Link href='/'>LOGO</Link>
            <Link href='/test1'>test1</Link>
            <Link href='/test2'>test2</Link>
        </div>
    )
}
