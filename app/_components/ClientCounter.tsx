'use client'

import {useState} from 'react'

export default function ClientCounter({text}: {text: string}) {
    const [counter, setCounter] = useState(0)

    const increment = () => {
        setCounter(c => c + 1)
    }

    return (
        <div className='flex flex-row m-4'>
            <div className='pr-2'>{counter}</div>
            <button className='bg-blue-600 p-2' onClick={increment}>{text}</button>
        </div>
    )
}
