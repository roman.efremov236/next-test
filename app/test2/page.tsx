import Button from '@/app/_components/Button'
import Post from '@/app/_components/Post'
import React, {Suspense} from 'react'
import PostText from '@/app/_components/PostText'
import {fetchPost} from '@/app/_lib/data'

export default async function Page() {
    const post = await fetchPost(0, 2)

    return (
        <div>
            <h2 className='text-xl'>Static text</h2>
            <div>
                <p>text</p>
                <p>another text</p>
            </div>
            <hr className='my-4'/>
            <Button text="button"/>
            <div>
                <Suspense fallback={<div className='text-red-500'>Loading...</div>}>
                    <Post delay={2000} id={post.id}/>
                </Suspense>
                <Suspense fallback={<div className='text-red-500'>Loading...</div>}>
                    <PostText text={post.title}/>
                </Suspense>
            </div>
        </div>
    )
}
